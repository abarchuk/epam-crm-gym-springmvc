package com.epam.restgym.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class AddTrainingRequest {
    private String traineeUsername;
    private String coachUsername;
    private String trainingName;
    private LocalDate trainingDate;
    private Integer durationInMinutes;
}