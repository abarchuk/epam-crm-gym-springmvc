package com.epam.restgym.dto;

public record CoachDto(
        String username,
        String firstname,
        String lastname,
        TrainingTypeDto specialization
) {
}
