package com.epam.restgym.dto;


public record RegisterCoachRequest(
        String firstname,
        String lastname,
        TrainingTypeDto specialization
) {

}
