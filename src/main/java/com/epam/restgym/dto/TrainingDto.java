package com.epam.restgym.dto;

import java.time.LocalDate;

public record TrainingDto(
        Long id,
        CoachDto coach,
        TrainingTypeDto trainingType,
        String trainingName,
        LocalDate trainingDate,
        Integer durationInMinutes
) {
}
