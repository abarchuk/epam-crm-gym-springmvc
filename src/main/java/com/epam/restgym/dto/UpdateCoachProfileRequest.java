package com.epam.restgym.dto;

import java.util.List;

public record UpdateCoachProfileRequest(
        String username,
        String firstname,
        String lastname,
        TrainingTypeDto specialization
) {
}
