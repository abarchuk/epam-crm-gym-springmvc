package com.epam.restgym.dto;

public record RegisterResponse(
        String username,
        String password
) {
}
