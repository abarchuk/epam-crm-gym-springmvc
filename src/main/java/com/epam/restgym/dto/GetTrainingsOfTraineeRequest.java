package com.epam.restgym.dto;


import java.time.LocalDateTime;

public record GetTrainingsOfTraineeRequest(
        String username,
        LocalDateTime periodFrom,
        LocalDateTime periodTo,
        String coachUsername,
        TrainingTypeDto trainingType
) {
}


