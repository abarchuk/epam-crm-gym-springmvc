package com.epam.restgym.repository;

import com.epam.restgym.model.User;
import lombok.extern.java.Log;
import org.springframework.stereotype.Repository;


import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.Root;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
@Log
@Transactional
public class UserRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public User save(User entity) {
        if (entityManager.contains(entity)) {
            entity = entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }

        return entity;
    }


    public List<User> findAll() {
        var criteria = entityManager.getCriteriaBuilder().createQuery(User.class);
        criteria.from(User.class);

        return entityManager.createQuery(criteria)
                .getResultList();

    }

    public Optional<User> getUserByUsername(String username) {
        var criteriaBuilder = entityManager.getCriteriaBuilder();
        var criteria = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        criteria.select(root)
                .where(criteriaBuilder.equal(root.get("username"), username));

        User result = null;
        try {
            result = entityManager.createQuery(criteria).getSingleResult();
        } catch (NoResultException e) {
            log.warning("No User was found by this username: " + username);
        }
        return Optional.ofNullable(result);
    }
}
