package com.epam.restgym.repository;

import com.epam.restgym.model.Trainee;
import com.epam.restgym.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class TraineeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Trainee save(Trainee entity) {
        if (entityManager.contains(entity)) {
            entity = entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }

        return entity;
    }

    public void delete(Trainee trainee) {
        entityManager.remove(trainee);
        entityManager.flush();
    }

    public Optional<Trainee> findById(Long id) {
        return Optional.ofNullable(entityManager.find(Trainee.class, id));
    }


    public List<Trainee> findAll() {
        var criteria = entityManager.getCriteriaBuilder().createQuery(Trainee.class);
        criteria.from(Trainee.class);

        return entityManager.createQuery(criteria)
                .getResultList();
    }


    public Optional<Trainee> getTraineeByUsername(String username) {
        var criteriaBuilder = entityManager.getCriteriaBuilder();
        var criteriaQuery = criteriaBuilder.createQuery(Trainee.class);

        Root<Trainee> traineeRoot = criteriaQuery.from(Trainee.class);
        Join<Trainee, User> userJoin = traineeRoot.join("user"); // Assuming "user" is the field in Trainee class.

        criteriaQuery.select(traineeRoot)
                .where(criteriaBuilder.equal(userJoin.get("username"), username));

        TypedQuery<Trainee> query = entityManager.createQuery(criteriaQuery);

        // Assuming there should be only one Trainee for a given username. If not, consider handling multiple results.
        return query.getResultList().stream().findFirst();
    }
}