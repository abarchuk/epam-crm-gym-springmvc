package com.epam.restgym.repository;

import com.epam.restgym.model.TrainingType;
import org.springframework.stereotype.Repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TrainingTypeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public TrainingType save(TrainingType entity) {
        if (entityManager.contains(entity)) {
            entity = entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }

        return entity;
    }


    public List<TrainingType> findAll() {
        var criteria = entityManager.getCriteriaBuilder().createQuery(TrainingType.class);
        criteria.from(TrainingType.class);

        return entityManager.createQuery(criteria)
                .getResultList();
    }
}
