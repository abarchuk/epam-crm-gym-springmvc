package com.epam.restgym.controller;

import com.epam.restgym.dto.*;
import com.epam.restgym.model.Coach;
import com.epam.restgym.model.Training;
import com.epam.restgym.service.TrainingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trainings")
@AllArgsConstructor
public class TrainingController {
    private final TrainingService trainingService;

    @GetMapping("/coach/{username}")
    public ResponseEntity<List<Coach>> getNotAssignedOnTraineeCoaches(@PathVariable(name = "username") String username) {
        var response = trainingService.getNotAssignedOnTraineeCoaches(username);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/trainee")
    public ResponseEntity<List<Training>> getTraineeTrainings(@RequestBody GetTrainingsOfTraineeRequest request) {
        var response = trainingService.getTraineeTrainings(request);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/coach")
    public ResponseEntity<List<Training>> getCoachTrainings(@RequestBody GetTrainingsOfCoachRequest request) {
        var response = trainingService.getCoachTrainings(request);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> addTraining(@RequestBody AddTrainingRequest request) {
        trainingService.addTraining(request);

        var response = ApiResponse.builder()
                .message("training added successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<TrainingTypeDto>> getTrainingTypes() {
        return ResponseEntity.ok(trainingService.getTrainingTypes());
    }
}
