package com.epam.restgym.controller;

import com.epam.restgym.dto.*;
import com.epam.restgym.model.Trainee;
import com.epam.restgym.service.TraineeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trainees")
@AllArgsConstructor
public class TraineeController {

    private final TraineeService traineeService;

    @PostMapping
    public ResponseEntity<RegisterResponse> traineeRegister(@RequestBody RegisterTraineeRequest request) {
        var response = traineeService.registerTrainee(request);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{username}")
    public ResponseEntity<Trainee> traineeProfile(@PathVariable(name = "username") String username) {
        var response = traineeService.getTraineeByUsername(username);

        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<Trainee> updateTraineeProfile(@RequestBody UpdateTraineeProfileRequest request) {
        var response = traineeService.updateTraineeProfile(request);

        return ResponseEntity.ok(response);
    }


    @DeleteMapping("/{username}")
    public ResponseEntity<ApiResponse> deleteTraineeProfile(@PathVariable(name = "username") String username) {
        traineeService.deleteTrainee(username);
        var response = ApiResponse.builder()
                .message("trainee deleted successfully")
                .success(true)
                .build();

        return ResponseEntity.ok(response);

    }

    @PatchMapping
    public ResponseEntity<ApiResponse> activateDeactivateTrainee(@RequestBody ActivateDeactivate activateDeactivate) {
        traineeService.activateDeactivateTrainee(activateDeactivate);
        boolean isActive = activateDeactivate.isActive();
        var response = ApiResponse.builder()
                .message(isActive ? "trainee activated successfully" : "trainee deactivated successfully")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

}
