alter table if exists coach
    drop constraint if exists FKm4k7f68bttmcfwvpie0fprrdm;

alter table if exists coach
    drop constraint if exists FKggj3lbwo8fwqcicu4fdbr0t49;

alter table if exists trainee
    drop constraint if exists FKpykyhhxyw9ooig1jxyrwcgkle;

alter table if exists training
    drop constraint if exists FKe00hj3snley9f46qjq09wibe4;

alter table if exists training
    drop constraint if exists FKi2dctw34e0xl50d8tsnrre7te;

alter table if exists training
    drop constraint if exists FKosdbocw0x9ygfmna67s7vtewh;

drop table if exists coach cascade;

drop table if exists trainee cascade;

drop table if exists training cascade;

drop table if exists training_type cascade;

drop table if exists users cascade;

create table coach
(
    id               bigserial not null,
    training_type_id bigint unique,
    user_id          bigint unique,
    primary key (id)
);

create table trainee
(
    dateOfBirth date,
    id          bigserial not null,
    user_id     bigint unique,
    primary key (id)
);

create table training
(
    durationInMinutes integer,
    trainingDate      date,
    coach_id          bigint,
    id                bigserial not null,
    trainee_id        bigint,
    training_type_id  bigint unique,
    trainingName      varchar(255),
    primary key (id)
);

create table training_type
(
    id               bigserial not null,
    trainingTypeName varchar(255) check (trainingTypeName in ('BODYBUILDING', 'SWIMMING', 'FITNESS', 'WORKOUT', 'BOXING', 'TAEKWONDO', 'KARATE', 'JUJITSU')),
    primary key (id)
);
create table users
(
    isActive  boolean,
    id        bigserial not null,
    firstname varchar(255),
    lastname  varchar(255),
    password  varchar(255),
    username  varchar(255) unique,
    primary key (id)
);

alter table if exists coach
    add constraint FKm4k7f68bttmcfwvpie0fprrdm
        foreign key (training_type_id)
            references training_type;

alter table if exists coach
    add constraint FKggj3lbwo8fwqcicu4fdbr0t49
        foreign key (user_id)
            references users;

alter table if exists trainee
    add constraint FKpykyhhxyw9ooig1jxyrwcgkle
        foreign key (user_id)
            references users;

alter table if exists training
    add constraint FKe00hj3snley9f46qjq09wibe4
        foreign key (coach_id)
            references coach;

alter table if exists training
    add constraint FKi2dctw34e0xl50d8tsnrre7te
        foreign key (trainee_id)
            references trainee;

alter table if exists training
    add constraint FKosdbocw0x9ygfmna67s7vtewh
        foreign key (training_type_id)
            references training_type;